/*The MIT License (MIT)

Copyright (c) 2015 Pierre Marijon <pierre@marijon.fr>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "server.hpp"

server::server()
{
    this->default_init();
}

server::server(std::string config_filename)
{
    /*If failled use default value*/
    this->default_init();

    this->read_configfile(config_filename);
}

server& server::port(unsigned int port)
{
    this->m_port = port;

    return *this;
}

unsigned int server::port(void)
{
    return this->m_port;
}

server& server::host(std::string host)
{
    this->m_host = host;

    return *this;
}

server& server::remote_control(std::string filename)
{
    this->m_remote_control_filename = filename;

    return *this;
}

std::string server::remote_control(void)
{
    return this->m_remote_control_filename;
}

server& server::server_password(std::string password)
{
    this->m_server_password = password;

    return *this;
}

std::string server::server_password(void)
{
    return this->m_server_password;
}

server& server::server_username(std::string username)
{
    this->m_server_username = username;

    return *this;
}

std::string server::server_username(void)
{
    return this->m_server_username;
}

server& server::client_password(std::string password)
{
    this->m_client_password = password;

    return *this;
}

std::string server::client_password(void)
{
    return this->m_client_password;
}

server& server::client_username(std::string username)
{
    this->m_client_username = username;

    return *this;
}

std::string server::client_username(void)
{
    return this->m_client_username;
}

void server::default_init(void)
{
    this->m_port = 5223;
    this->m_host = "0.0.0.0";
    this->m_remote_control_filename = "";
    this->m_server_password = "slave_password";
    this->m_server_username = "slave";
    this->m_client_password = "master_password";
    this->m_client_username = "master";
}

void server::read_configfile(std::string config_filename)
{
    // do nothing yet
    config_filename = "";
}
