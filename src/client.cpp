/*The MIT License (MIT)

Copyright (c) 2015 Pierre Marijon <pierre@marijon.fr>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "client.hpp"

client::client()
{
    this->default_init();
}

client::client(std::string config_filename)
{    
    /*If failled use default value*/
    this->default_init();

    this->read_configfile(config_filename);
}

client& client::password(std::string password)
{
    this->m_password = password;

    return *this;
}

std::string client::password(void)
{
    return this->m_password;
}

client& client::username(std::string username)
{
    this->m_username = username;

    return *this;
}

std::string client::username(void)
{
    return this->m_username;
}

client& client::server_host(std::string host)
{
    this->m_server_host = host;

    return *this;
}

std::string client::server_host(void)
{
    return this->m_server_host;
}

void client::default_init(void)
{
    this->m_password = "master_password";
    this->m_username = "master";
    this->m_server_host = "0.0.0.0";
}

void read_configfile(std::string config_filename)
{
    // do nothing yet
    config_filename = "";
}






