TEMPLATE = lib
VERSION = 0.1
TARGET = yarc

# QXmpp import
include(qxmpp/qxmpp.pri)
INCLUDEPATH += $$QXMPP_INCLUDEPATH
LIBS += -L$$PWD/qxmpp/src $$QXMPP_LIBS

SUBDIRS += qxmpp

# Define project file
SOURCES += src/*
HEADERS += inc/*

# Define include path
INCLUDEPATH += inc/

# Define output directory
OBJECTS_DIR = $$PWD/build
MOC_DIR = $$OBJECT_DIR

DESTDIR = $$PWD/lib
              

