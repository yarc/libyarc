/*The MIT License (MIT)

Copyright (c) 2015 Pierre Marijon <pierre@marijon.fr>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef __HG_CLIENT_HPP__
#define __HG_CLIENT_HPP__

/*standard include*/
#include <string>

class client
{
public:
    
    client();
    client(std::string config_filename);

    /*Getter, Setter and named parameter*/
    client& password(std::string password);
    std::string password(void);

    client& username(std::string username);
    std::string username(void);
    
    client& server_host(std::string host);
    std::string server_host(void);

    /*Working method*/
    bool connect(void);
    bool send_message(std::string message);

private:

    void default_init(void);
    void read_configfile(std::string config_filename);
    
private:

    std::string m_password;
    std::string m_username;
    std::string m_server_host;
};

#endif /*__HG_CLIENT_HPP__*/
