/*The MIT License (MIT)

Copyright (c) 2015 Pierre Marijon <pierre@marijon.fr>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef __HG_SERVER_HPP__
#define __HG_SERVER_HPP__

/*standard include*/
#include <string>

class server
{
public:

    /*Constructor*/
    server();
    server(std::string config_filename);

    /*Getter, Setter and named parameter*/
    server& port(unsigned int port = 5223);
    unsigned int port(void);

    server& host(std::string host);
    std::string host(void);

    server& remote_control(std::string filename);
    std::string remote_control(void);

    server& server_password(std::string password);
    std::string server_password(void);

    server& server_username(std::string password);
    std::string server_username(void);

    server& client_password(std::string password);
    std::string client_password(void);

    server& client_username(std::string password);
    std::string client_username(void);

    
    /*Working method*/
    bool start(void);
    bool send_message(void);

private:

    void default_init(void);
    void read_configfile(std::string config_filename);
    
private:

    unsigned int m_port;
    std::string m_host;
    std::string m_remote_control_filename;
    std::string m_server_password;
    std::string m_server_username;
    std::string m_client_password;
    std::string m_client_username;
};

#endif // __HG_SERVER_HPP__



